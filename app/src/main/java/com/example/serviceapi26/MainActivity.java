package com.example.serviceapi26;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";
    private EditText txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txt = findViewById(R.id.Edittxt);
    }
    //Foreground services
    public void startService(View v){
        String input = txt.getText().toString();

        Intent serviceIntent = new Intent(this, ForegroundService.class);
        serviceIntent.putExtra("key", input);

        ContextCompat.startForegroundService(this, serviceIntent);
    }
    public void stopService(View v){
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        stopService(serviceIntent);
    }

    //Bound services
    public void BindService(View v){
        Intent bsIntent = new Intent(this, BoundService.class);
        bindService(bsIntent, serviceConnection, BIND_AUTO_CREATE);
    }
    public void UnbindService(View v){
        unbindService(serviceConnection);
    }
    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "Service Bound");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "Service unexpectedly stopped");
        }
    };
}