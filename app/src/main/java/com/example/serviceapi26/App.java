package com.example.serviceapi26;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;

public class App extends Application {
    public static final String Channel_ID = "Channel1";
    @Override
    public void onCreate(){
        super.onCreate();

        createNotificationChannel();
    }
    private void createNotificationChannel(){
        NotificationChannel serviceChannel = new NotificationChannel(
                Channel_ID,
                "ServiceChannel",
                NotificationManager.IMPORTANCE_DEFAULT
        );
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(serviceChannel);
    }
}
