package com.example.serviceapi26;

import static com.example.serviceapi26.App.Channel_ID;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class ForegroundService extends Service {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String txt = intent.getStringExtra("key");
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent,PendingIntent.FLAG_IMMUTABLE);

        Notification notification = new NotificationCompat.Builder(this, Channel_ID)
                .setContentTitle("ForegroundService")
                .setContentText(txt)
                .setSmallIcon(R.drawable.ic_fg)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
        //when job done call stopself() to stop service
        //stopSelf();

        return START_NOT_STICKY;
    }

    //for bound services
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
