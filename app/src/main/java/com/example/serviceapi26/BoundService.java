package com.example.serviceapi26;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import java.security.Provider;

public class BoundService extends Service {
    public static final String TAG = "BoundService";
    private IBinder myBinder = new MyBinder();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        DoStuff();
        return myBinder;
    }
    public class MyBinder extends Binder{
        BoundService getService(){
            return BoundService.this; //singleton
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "Unbound");
        return super.onUnbind(intent);
    }

    public void DoStuff(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Job started");
            }
        }).start();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        stopSelf(); //stops service when client closes
    }
}
